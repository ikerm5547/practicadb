/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica001poo;

/**
 *
 * @author Iker Martinez
 */
public class Terreno {
    private int id;
    private int numTerreno;
    private float ancho;
    private float largo;
    private int status;
            
 //constructores            
public Terreno(){
this.id =0;
this.numTerreno=0;
this.ancho=0.0f;
this.largo=0.0f;
this.status = 0;
}
public Terreno( int id,int numTerreno, float ancho, float largo,int status){
    this.id=id;
    this.numTerreno=numTerreno;
    this.ancho=ancho;
    this.largo=largo;
    this.status =status;
}
public Terreno(Terreno otro){
    id=otro.id;
    this.numTerreno=otro.numTerreno;
    this.largo=otro.largo;
    this.ancho=otro.ancho;
    status=otro.status;
}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

//metodos get y set 
    public int getNumTerreno() {
        return numTerreno;
    }

    public void setNumTerreno(int numTerreno) {
        this.numTerreno = numTerreno;
    }

    public float getAncho() {
        return ancho;
    }

    public void setAncho(float ancho) {
        this.ancho = ancho;
    }

    public float getLargo() {
        return largo;
    }

    public void setLargo(float largo) {
        this.largo = largo;
    }

public float calculadoraPerimetro(){
float perimetro=0.0f;
perimetro = (this.ancho + this.largo)*2;
return perimetro;
}

public float calculadoraArea(){
float area = 0.0f;
area = this.ancho * this.largo;
return area;
}


}


